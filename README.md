# Details
This is a fork of gst-ffmpeg 0.10.13 that has been patched to fix [this bug](http://git.buildroot.net/buildroot/commit/?id=6ea49a23df3fa950e253bf704188e87a583a8ba4) when compiling.

This package is required for using QtMultimedia libraries on Ubuntu 14.04. Unfortunately Qt5 still relies on GStreamer 0.10 and the older gst-ffmpeg instead of the newer GStreamer 1.0 and gst-libav. Ubuntu 14.04 however no longer includes gst-ffmpeg for GStreamer 0.10, so the package needs to be compiled and installed manually.

# Building

```sh
$ cd gst-ffmpeg
$ ./configure --prefix=/usr --with-ffmpeg-extra-configure="--enable-nonfree"
$ make
$ sudo check install
```

This should produce a .deb file that you can than install using dpkg

```sh
$ sudo dpkg -i gst-ffmpeg_0.10.13-1_amd64.deb
```
